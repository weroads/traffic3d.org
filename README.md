# traffic3d.org

[![pipeline status](https://gitlab.com/traffic3d/traffic3d.org/badges/master/pipeline.svg)](https://gitlab.com/traffic3d/traffic3d.org/-/commits/master)
[![Documentation Status](https://readthedocs.org/projects/traffic3d/badge/?version=latest)](https://traffic3d.org/en/latest/?badge=latest)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3968432.svg)](https://doi.org/10.5281/zenodo.3968432)

Source code for the traffic3d.org website.
This site is build with [mkdocs](https://www.mkdocs.org/) and the [material](https://squidfunk.github.io/mkdocs-material/) theme.

## Developers

Current developers:

* Callum Bugajski - @callumbugajski via [Beautiful Canoe](https://beautifulcanoe.com/)

## License

This software is licensed under the [Mozilla Public License Version 2.0](/LICENSE).
Copies of the license can also be obtained [directly from Mozilla](https://mozilla.org/MPL/2.0/).

## Getting Started with Development

Please read [CONTRIBUTING.md](/CONTRIBUTING.md) before you start working on this repository.
