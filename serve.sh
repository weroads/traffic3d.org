#!/bin/sh

export CI_ENVIRONMENT_URL="127.0.0.1:8888"
export CI_PROJECT_NAME="traffic3d.org"
export CI_PROJECT_URL="https://gitlab.com/traffic3d/traffic3d.org"

if test -w /var/run/docker.sock; then
    DOCKER="docker"
else
    echo "Using 'sudo docker' to run Docker. Please add yourself to the docker group with:"
    echo
    echo "    sudo usermod -aG docker ${USER}"
    DOCKER="sudo docker"
fi

"${DOCKER}" run --rm -it --user $(id -u):$(id -g) -p 8888:8000 -v /etc/passwd:/etc/passwd -v ${PWD}:/docs squidfunk/mkdocs-material:8.3.9 $@

unset CI_ENVIRONMENT_URL
unset CI_PROJECT_NAME
unset CI_PROJECT_URL
