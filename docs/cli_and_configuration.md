# CLI and configuration

## CLI options

Traffic3D has custom command line options using following flag:
`-executeMethod CustomCommandLineArguments.Run`

The custom options are:

* `-JSONConfigFile "filename.json"` - Import settings from a JSON file (see below for config details)
* `-OpenScene "Scenes/sceneName.unity"` - Open a scene
* `-RunBenchmark true|false` - Run Benchmark

### Examples

```sh
# Using all custom options.
${UNITY_EXECUTABLE:-xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' /opt/Unity/Editor/Unity} \
  -projectPath $(pwd)/Traffic3D \
  -testPlatform playmode \
  -testResults $(pwd)/playmode-results.xml \
  -executeMethod CustomCommandLineArguments.Run \
  -JSONConfigFile "config.json" \
  -OpenScene "Scenes/NightDemo.unity" \
  -RunBenchmark true \
  -logFile \
  -batchmode
```

## Configuration

Configuration is used to bulk import initialisation values into the Traffic3D system.
Normally the values are direct mappings of the values that can be changed within the Unity UI.
For example the `vehicleFactoryConfig` object within the config example below allows us to pass data directly to the specified fields.
e.g. The `highRangeRespawnTime` field in the config maps to the `highRangeRespawnTime` field in the game object.

### Examples

See [test_config.json](https://gitlab.com/traffic3d/traffic3d/-/blob/develop/Traffic3D/Assets/Tests/TestFiles/test_config.json) for an example of how to structure the file.
