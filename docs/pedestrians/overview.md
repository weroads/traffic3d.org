# Pedestrians Overview

Traffic3D contains two pedestrian systems, the Traffic3D [pedestrian system](traffic3d_pedestrians.md) and the more advanced [Evacu-agent system](evacu-agent/introduction.md).

## Which system should I use?

### Traffic3D pedestrians

The Traffic3D system is quicker and easier to setup, but it is not as customizable as Evacu-agent.
Pedestrians are removed from the simulation once they reach their random destinations, and new pedestrians are spawned.
Whereas Evacu-agent pedestrians persist for the whole length of the simulation.
[**Click here**](traffic3d_pedestrians.md#setup) to find out how to setup the Traffic3D pedestrian system.

### Evacu-agent pedestrians

The Evacu-agent system allows for custom pedestrian behaviour to be added and edited, as described [here](evacu-agent/add_a_new_pedestrian.md).
Evacu-agent pedestrians behave in a more realistic manner.
Different pedestrians (or groups of pedestrians) can have different destinations and behaviour, for example e.g. `Workers` will navigate towards a work-related location.
[**Click here**](evacu-agent/setup.md) to find out how to setup the Evacu-agent pedestrian system.
