# Traffic3D Pedestrians

## Assets

### Pedestrian Point

Transforms the position at which pedestrians spawn / go to with the `PedestrianPoint.cs` script attached.

### Pedestrians

Pedestrians will walk around the scene by spawning at `Pedestrian Points` and moving towards another `Pedestrian Point` as their destination.
Models can be created and edited.
The `Pedestrian Factory` will allow multiple models to be added with a probability of the model spawning.

![Pedestrian Configuration](../assets/images/PedestrianConfig.png)

The `Pedestrian.cs` Script has the following fields:

* **Walking In Road Percentage** - The chance of the pedestrian walking into the road to reach their destination.
* **Partial Path Fallback Location Distance** - This probably won't be edited as much but this is the distance a pedestrian will look around for an original path node to walk back to if their path is suddenly partial (which mainly happens when crossings are turned off) almost like a safe spot for the pedestrian to stand. If no original nodes are found, then they will stand where they are.
* **Max Speed** - This is the maximum range of speed they will start with.
* **Min Speed** - The minimum speed.
* **Probability of Running** - The probability that the pedestrian's speed will be over the set running speed.

There is also a `Nav Mesh Agent` Component that can be edited too (except for speed as that is decided by the Pedestrian script) which can be found here: [https://docs.unity3d.com/Manual/class-NavMeshAgent.html](https://docs.unity3d.com/Manual/class-NavMeshAgent.html)

When creating a pedestrian there are examples within the `Assets/Resources/Models/Pedestrians/` folder which can be cloned and used to create new models.

### Pedestrian Crossings

The crossing is used by pedestrians to allow them to cross at relevant times.
A pedestrian will wait until the crossing allows the pedestrian to cross which are normally changed in a [**Junction State**](#junctions).
`PedestrianCrossing.cs` has the following fields:

* **Pedestrian Crossing ID** – A unique ID to identify the crossing from others.
* **Pedestrian Currently in Crossing Area** - The current pedestrians in the area of the crossing.
* **Allow Crossing** - A boolean, when true, allows pedestrians to cross roads.

### Pedestrian Factory

Creates the pedestrians in the simulation, the script has the following fields:

![Pedestrian Factory Configuration](../assets/images/PedestrianFactoryConfig.png)

* **Low Range Respawn Time** - Lower range for a random time to be generated for the next pedestrians to spawn in simulation seconds (similar to Vehicle respawn time).
* **High Range Respawn Time** - The upper range of above in simulation seconds.
* **Maximum Pedestrian Count** - The max number of pedestrians that can be in the simulation at once.
* **Pedestrian Probabilities** (*List*) - A list of pedestrian models with the percentage probability of them spawning next. Use this add extra Pedestrian Models.
    * **Pedestrian** - The Pedestrian.
    * **Probability** - The probability between `0.0 - 1.0` of the pedestrian spawning.

### Pedestrian Manager

This mainly runs in the background, determining which surfaces are roads or pathways for the pedestrians.
It also holds a list of the crossings.

## Setup

1. Firstly, check out the `DayDemo` scene for an example of these pedestrians.
1. In your scene, create a new game object, call it `PedestrianFactory` and add the `PedestrianFactory` script.
1. Fill in the values using the [Pedestrian Factory](#pedestrian-factory) section above.
1. Make sure there is at least one pedestrian in the `Pedestrian Probabilities` list for pedestrians to spawn.
1. Create a path by using one of the prefabs in `Assets/Resources/Models` or create your own game object and tag it with the `pathway` tag.
1. Create another game object, call it `PedestrianPoint1`, add the `PedestrianPoint` script and move its position to place it just over the end of a path.
1. Copy the same game object and call it `PedestrianPoint2` and move its position to the other side of the path.
1. Run the simulation and pedestrians should spawn and walk to the other side of the path before being destroyed.
