# Evacu-Agent: A crowd simulator with individual agent behaviours

Evacu-agent is a crowd simulation tool that extends Traffic3D and was built to
facilitate the creation of simulations to display both crowding behaviour and
the effect that individual, non-uniform, behaviour has on crowd dynamics.
Evacu-Agent also provides a graphical view of this behaviour, allowing for
simpler study of emergent behaviour phenomena.

Evacu-agent is based on the [Unity 3d games engine](https://unity3d.com/unity).
All logic is written in [C#](https://www.docs.microsoft.com/en-us/dotnet/csharp/)
using [Unity scripting](https://docs.unity3d.com/Manual/ScriptingSection.html).

## Supported platforms

Evacu-agent is tested on 64-bit Windows, Linux and OSX.

## Download Evacu-Agent

As Evacu-agent resides in Traffic3D, instructions for download can be found on
[Traffic3D](../../index.md)s homepage.

## Main components

Evacu-agent has three main components:

1. Boids-like flocking behaviour,
1. A behaviour hierarchy; and
1. A field-of-view system.

## Entry point to Evacu-agent through Traffic3d

All entry into Evacu-agent begins in `PedestrianFactory.cs`, more details can be found [here](add_a_new_pedestrian.md).
