# Adding a new Pedestrians

## Entry point into Evacu-agent logic from Traffic3D

The entry point that initiates Evacu-agent logic in Traffic3D can be found in `PedestrianFactory`.

The method `SpawnPedestrian()` instantiates a regular Traffic3D pedestrian consisting of:

1. A humanoid pedestrian model.
1. Walking and running animations.
1. Textures for clothing and facial features.
1. A `Pedestrian` script.

The `Pedestrian` script for Traffic3D allows `Pedestrian`s to spawn and then walk or run to a randomly chosen junction.

Upon reaching the road junction they have chosen to cross, the `Pedestrian` will wait for the appropriate traffic light signal to cross.

After crossing the pedestrian walks to a location and de-spawns.

Evacu-agent logic is then added to the Traffic3D pedestrian and the boolean `isUsingEvacuationBehaviour` in `Pedestrian` set to true to turn off Traffic3D pedestrian behaviour so the two sets of logic do not interfere.

## Pedestrian types

Pedestrians are divided into Types.

For example:

1. `FriendGroupLeaderPedestrian`.
1. `FriendGroupFollowerPedestrian`.

Each different type models different pedestrians you may see in a high street setting.

The examples above model friend groups who will meet in a location on the map and then travel to various `BuildingPedestrianPointTypeBase` together.

`BuildingPedestrianPointTypeBase` are explained in more detail below.

Note the naming convention suffixes:

1. `LeaderPedestrian`.
1. `FollowerPedestrian`.

This is used as all pedestrians in Evacu-agent are in groups, with a group leader and followers.

Groups are explained in more detail below.

## Points of interest on the map for Pedestrians

`BuildingPedestrianPointTypeBase` are places on the map to act as markers for buildings such as food stores and offices.

`BuildingPedestrianPointTypeBase` extends `PedestrianPoint` found in Traffic3D with specific types such as `ShoppingPedestrianPoint` extending `BuildingPedestrianPointTypeBase`.

## Groups

All pedestrian types in Evacu-agent are in groups, which must contain one or more pedestrians.

Group logic is contained in `GroupCollection.cs` which is responsible for maintaining a `List<EvacuAgentPedestrianBase>` to represent the members of the group as well as storing the groups `List<Vector3> path` and `Vector3 GroupDestination`.

Only one `GroupCollection` is held by a group, and is placed on the group leader, with members receiving a reference to it.

By having only one `GroupCollection` per group the group is able to share the same path and path without updating many `GroupCollection` objects.

Pedestrians simply check the current destination in `GroupCollection` and make decisions from it, updating it when the `currentDestination` is reached.

`GroupCollection` is also used in grouping boid behaviours where pedestrians should apply different spacing behaviour to members of their own group than to members of other groups.

Boid grouping can be viewed in more detail on [Boid Grouping](boid_grouping.md).

## Adding a new Pedestrian type

Several parts are needed to add a new Pedestrian into Evacu-agent.

The necessary steps have been listed below and then explained in more detail in subsequent lists.

Parts necessary:

1. Add a new `Pedestrain` prefab.
1. Create a new `Pedestrian` script.
1. Create a new `BehaviourTypeOrder`.
1. Create a new `Pedestrian highlight`.
1. Add the necessary scripts to the new `Pedestrian` `GameObject` and set the appropriate values in the [Inspector window](https://docs.unity3d.com/Manual/UsingTheInspector.html).
1. Add a `AbstractEvacuAgentPedestrianFactory` to instantiate the `Pedestrian` at run time.
1. Add the new factory to `PedestrianFactory`.

### 1. Adding a new Pedestrian prefab object

Creating a prefab for each type of `Pedestrian` allows `AbstractEvacuAgentPedestrianFactory` to instantiate them at runtime without large amounts of code.

1. Create an empty `GameObject` in any scene by right-clicking the [Hierarchy](https://docs.unity3d.com/Manual/Hierarchy.html), an appropriate name suffixed with **LeaderPedestrian** or **FollowerPedestrian** should be chosen. ![Creating empty GameObject example](../../assets/images/EvacuAgent/CreateEmptyGameObjectForPrefab.png)
1. Drag and drop the new `GameObject` into `Asset/Resources/EvacuAgent/Prefabs/Pedestrian_Types` from the hierarchy window. ![Drag and drop of new prefab before](../../assets/images/EvacuAgent/DragAndDropNewGameObjectExample.png) ![Drag and drop of new prefab after](../../assets/images/EvacuAgent/DragAndDropAfterExample.png)
1. Double click the new `Pedestrian GameObject` to open the [prefab menu](https://docs.unity3d.com/Manual/EditingInPrefabMode.html) and ensure that each [Transform](https://docs.unity3d.com/ScriptReference/Transform.html) component is set to **0** and that each [Scale](https://docs.unity3d.com/ScriptReference/Transform-localScale.html) is set to **1** in the [Inspector window](https://docs.unity3d.com/Manual/UsingTheInspector.html). ![New pedestrian transform example](../../assets/images/EvacuAgent/NewPedestrianTransform.png)

### 2. Create a new Pedestrian script

Creating new scripts for each `Pedestrian` has a similar benefit to creating prefabs of each pedestrian in that each can share a large number of properties through extending `EvacuAgentPedestrianBase`.

1. Navigate to `Assets/Scripts/EvacuAgent/PedestrianTypes` in the [Project window](https://docs.unity3d.com/Manual/ProjectView.html), right click inside the `PedestrianTypes` folder and create a new `C# script`. ![Creating a new Pedestrian script example](../../assets/images/EvacuAgent/CreatingNewPedestrianScript.png)
1. Double click the script to open it in your IDE and set the script to extend `NonShooterPedestrian`. ![New Pedestrian script example](../../assets/images/EvacuAgent/NewPedestrianScriptExample.png)
1. If creating a group leader `Pedestrian` you will also need to create a path creator script in `Assets/Scripts/EvacuAgent/PathGenerators` that should extend `NonShooterPedestrianPointPathCreator`.
1. The logic for the new path creator can utilise both `PedestrianPoint` locations or random valid locations on the [NavMesh](https://docs.unity3d.com/Manual/nav-BuildingNavMesh.html).
1. `WorkerPedestrianPointPathCreator` is shown as an example below, it simulates a `Pedestrian` walking to work with a random chance of stopping for coffee before work.
1. Only group leaders decide on paths for the group currently so non-leader group members do not require a path creator script.

```csharp
public class WorkerPedestrianPointPathCreator : NonShooterPedestrianPointPathCreator
{
    public override List<Vector3> CreatePath()
    {
        List<Vector3> path = new List<Vector3>();

        if (UnityEngine.Random.value < EvacuAgentSceneParamaters.WORKER_CHANCE_TO_VISIT_HOSPITALITY_POINT_IN_ROUTE)
        {
            path.Add(GetRandomPedestrianPointOfType(PedestrianPointType.Hospitality).GetPointLocation());
        }

        path.Add(GetRandomPedestrianPointOfType(PedestrianPointType.Work).GetPointLocation());

        return path;
    }
}
```

### 3. Create a new behaviour type order

1. See [Behaviour structure](behaviour_structure.md).

### 4. Create Pedestrian highlight

Each `Pedestrian` is given a highlight, modelled by a small flat circle that appears at the base of the pedestrian feet. This can be toggled by the UI at runtime to more easily distinguish `Pedestrian` types.

1. Create a new empty `GameObject` the same as in Step 1 with the name prefixed with the pedestrian type and suffixed with the word **Highlight**. For example: `ExampleLeaderHighlight`.
1. This new `GameObject` should be dragged into `Asset/Resources/EvacuAgent/Prefabs/Pedestrian_Highlights`.
1. Create a new [Material](https://docs.unity3d.com/ScriptReference/Material.html) by right-clicking in `Asset/Resources/EvacuAgent/Materials`. ![New material example](../../assets/images/EvacuAgent/NewMaterialExample.png)
1. Change the colour of the new material via the [Albedo](https://docs.unity3d.com/Manual/StandardShaderMaterialParameterAlbedoColor.html) property, the colour scheme is a darker shade of the same colour for leaders and a lighter shade of the same colour for non-leader members. ![New material colour example](../../assets/images/EvacuAgent/NewMaterialColour.png)
1. Open the prefab for the new highlight created in Step 4.2 and apply the material created in Step 4.4 in the material property of the [Mesh Renderer](https://docs.unity3d.com/Manual/class-MeshRenderer.html), shown in red below. This change should be reflected in the shader property marked in blue. ![Setting highlight material example](../../assets/images/EvacuAgent/SettingHighlightMaterial.png)
1. Add a tag to the highlight following a naming pattern of: `PedestrianType` `Highlight`. For example: `exampleHighlight`. Note that adding a tag will not apply it to the currently open prefab, a new tag must be created and then manually applied to the new highlight prefab. ![Add tag to highlight example](../../assets/images/EvacuAgent/AddTagToHighlight.png)
1. Add a string value to `EvacuAgentSceneParamaters` that matches the string input for the new tag (case-sensitive). For example: `public static string EXAMPLE_HIGHLIGHT = "exampleHighlight";`.

### 5. Add the necessary scripts to the new Pedestrian

1. Open the new pedestrian prefab again with the [Inspector window](https://docs.unity3d.com/Manual/UsingTheInspector.html) open.
1. Using the `Add Component` button add the pedestrian script created in step 2. ![Empty example Pedestrian prefab](../../assets/images/EvacuAgent/EmptyExamplePedestrianPrefab.png)
1. Populate the `Pedestrian` script as seen below remembering that non-leader group members do not require a `Pedestrian Point Path Creator` or `GroupCollection`. Scripts must be added via `Add component` and then dragged to the appropriate slot, prefabs can simply be dragged to the appropriate slot. ![Example Pedestrian inspector setup](../../assets/images/EvacuAgent/ExamplePedestrianInspectorSetup.png)
1. Please note that `BehaviourStrategy` scripts and `BoidComponent` scripts also need to be added via `Add Component` here but have been omitted for brevity.
1. Add a new tag such as `example` and apply it to the new Pedestrian prefab.
1. Add the same string used in the new pedestrian tag to `EvacuAgentSceneParamaters` for example `public static string EXAMPLE_TAG = "example";`.

### 6. Add a factory to instantiate the pedestrian

1. Add a new script to `Asset/Scripts/EvacuAgent/Factories` with the naming convention `PedestrianType` `LeaderFollowerPedestrianFactory`. ![Add Pedestrian factory example](../../assets/images/EvacuAgent/AddPedestrianFactoryExample.png)
1. Open the new factory script in your IDE and make it extend `AbstractEvacuAgentPedestrianFactory`.
1. The factories are similar enough that the methods `Awake()` and `CreateEvacuAgentPedestrian` can be copied into the new factory script. The items that will need to be changed can be seen below. ![New factory items to change](../../assets/images/EvacuAgent/NewFactoryScriptItemsToChange.png)
1. `Blue` shows the maximum number of groups the factory can instantiate. This needs to be changed by adding `public static int NUMBER_OF_EXAMPLE_AGENTS` with an `int` value to `EvacuAgentSceneParamaters` and then changing the new factory script file to the property added in `EvacuAgentSceneParamaters`.
1. `Red` and `Yellow` is a bool for if the highlights for this Pedestrian type are currentlytoggled on so that new pedestrians spawned will have the highlight enabled on spawn. This needs to be changed by adding `public static bool IS_EXAMPLE_HIGHTLIGHT_VISUAL_ENABLED` with a `false` value to `EvacuAgentSceneParamaters` and then changing the new factory script file to the property added in `EvacuAgentSceneParamaters` for both references.
1. `Green` is the minimum and maximum number of followers or non-leader members a group can have. This needs to be changed by adding `public static int EXAMPLE_GROUP_FOLLOWER_COUNT_MINIMUM` and `public static int EXAMPLE_GROUP_FOLLOWER_COUNT_MAXIMUM` with a `int` values to `EvacuAgentSceneParamaters` and then changing the new factory script file to the properties added in `EvacuAgentSceneParamaters`.
1. A fully worked example can be seen below:

```csharp
public class ExampleLeaderFollowerPedestrianFactory : AbstractEvacuAgentPedestrianFactory
{
    private void Awake()
    {
        numPedestriansToSpawn = EvacuAgentSceneParamaters.NUMBER_OF_EXAMPLE_AGENTS;
        numberOfFollowersLeftToSpawn = 0;
    }

    public override EvacuAgentPedestrianBase CreateEvacuAgentPedestrian(Pedestrian pedestrian)
    {
        if (numberOfFollowersLeftToSpawn == 0)
        {
            EvacuAgentPedestrianBase groupLeaderPedestrian = CreatePedestrianType(pedestrian, EvacuAgentSceneParamaters.IS_EXAMPLE_HIGHTLIGHT_VISUAL_ENABLED, leaderPedestrianTypePrefab);
            currentLeaderPedestrian = groupLeaderPedestrian;
            numberOfFollowersLeftToSpawn = GetNumberOfFollowersForCurrentGroup(EvacuAgentSceneParamaters.EXAMPLE_GROUP_FOLLOWER_COUNT_MINIMUM, EvacuAgentSceneParamaters.EXAMPLE_GROUP_FOLLOWER_COUNT_MAXIMUM);
            return UpdateGroupCollection();
        }

        EvacuAgentPedestrianBase groupFollowerPedestrian = CreatePedestrianType(pedestrian, EvacuAgentSceneParamaters.IS_EXAMPLE_HIGHTLIGHT_VISUAL_ENABLED, followerPedestrianTypePrefab);
        AddGroupCollectionToFollower(groupFollowerPedestrian);
        return AssignToFollowerCollection(groupFollowerPedestrian);
    }
}
```

### 7. Add the new factory to PedestrianFactory

1. Any scene that spawns `Pedestrian` needs to have a single `GameObject` with a `PedestrianFactory` attached to it as well as specific EvacuAgent factories such as `WorkerLeaderFollowerPedestrianFactory`.
1. To enable new `Pedestrian` to be spawned use the `Add Component` button on the `PedestrianFactory` `GameObject` to add the factory made in Steps 5.1 to 5.6 dragging and dropping the appropriate prefabs as seen below. Note that pedestrians that are only have leaders are allowed but both boxes must be set as the leader pedestrian. ![Add new pedestrian factory to scene](../../assets/images/EvacuAgent/AddNewPedestrianFactoryToPedestrianFactoryGameObject.png)
1. In `PedestrianFactory.SetUpEvacuAgentPedestrianFactories` add `gameObject.GetComponent<ExampleLeaderFollowerPedestrianFactory>()`. Note that commenting these factories out allows you to spawn only specific `Pedestrian` types for easier debugging.

```csharp
private List<AbstractEvacuAgentPedestrianFactory> SetUpEvacuAgentPedestrianFactories()
  {
      return new List<AbstractEvacuAgentPedestrianFactory>()
      {
          gameObject.GetComponent<WorkerLeaderFollowerPedestrianFactory>(),
          gameObject.GetComponent<FriendGroupLeaderFollowerPedestrianFactory>(),
          gameObject.GetComponent<ExampleLeaderFollowerPedestrianFactory>()
      };
  }
```
