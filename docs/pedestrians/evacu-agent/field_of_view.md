# Field of view

Each pedestrian in Evacu-agent has a field of view that is used to gather information about other pedestrians in the simulation, for use in crowding and individual behaviours.

## How Field of view works

There are three stages to field of view:

1. All other pedestrians in a given radius are detected using [Physics.OverlapSphere](https://docs.unity3d.com/ScriptReference/Physics.OverlapSphere.html). Whereby all [colliders](https://docs.unity3d.com/ScriptReference/Collider.html) in the pedestrian [layer](https://docs.unity3d.com/Manual/Layers.html) are added to a collection in Field of view.

1. The pedestrians detected with [Physics.OverlapSphere](https://docs.unity3d.com/ScriptReference/Physics.OverlapSphere.html) are iterated over. In each case the angle and the distance to the target is calculated. If the angle to the target is less than the `viewAngle` and the distance to the target is less than `viewRadius` then the method continues.

1. [Raycasts](https://docs.unity3d.com/ScriptReference/Physics.Raycast.html) are then performed to each pedestrian in the detected collection to determine if the pedestrian is visible or if they are behind an obstacle belonging to the obstacle [layer](https://docs.unity3d.com/Manual/Layers.html). If the raycast intersects any [GameObject](https://docs.unity3d.com/ScriptReference/GameObject.html) belonging to the obstacle layer before it hits the target pedestrian then the target is behind an obstacle.

![FieldOfView diagram](../../assets/images/EvacuAgent/FieldOfViewExampleDiagram.png)

```csharp
public void GetAllAgentsInViewAngle()
{
    allVisiblePedestrians.Clear();
    Collider[] agentsInRadius = Physics.OverlapSphere(transform.position, viewRadius, targetLayer);

    for (int index = 0; index < agentsInRadius.Length; index++)
    {
        Transform agentTransform = agentsInRadius[index].transform;
        Vector3 angleToAgent = (agentTransform.position - transform.position).normalized;

        if (Vector3.Angle(transform.forward, angleToAgent) < viewAngle / 2)
        {
            float distanceToAgent = Vector3.Distance(transform.position, agentTransform.position);

            if(distanceToAgent > viewRadius)
                continue;

            // Ensure pedestrain does not detect self
            if (distanceToAgent == 0f)
                continue;

            RaycastHit raycastHit;
            if (!Physics.Raycast(transform.position, angleToAgent, out raycastHit, distanceToAgent, obstacleBitmask))
            {
                Debug.DrawRay(transform.position, agentTransform.position);
                allVisiblePedestrians.Add(agentTransform.GetComponentInParent<Pedestrian>());
            }
        }
    }
}
```

## View radius and view angle

The field of view has two properties for determining the size of the `OverlapSphere` used for step 1, which allows pedestrians to more realistically mimic human vision.
Some pedestrians are able to view other pedestrians from further away and with greater peripheral vision.

This allows the percepts that pedestrians generate to be non-uniform from one pedestrian to the next.

The properties are:

1. `ViewAngle` - This property is clamed using the [RangeAttribute](https://docs.unity3d.com/ScriptReference/RangeAttribute.html) to be between 0 and 360 inclusive to model a full radius around the pedestrian.
1. `ViewRadius` - This is unclamped.

At the time of writing both properties contain the same values for all pedestrians, but are public and readily available to be differed for each pedestrian in the future.

## Limitations

1. Whilst currently realistic enough, the raycasts are drawn from the feet of pedestrians. This means that in some cases the raycast can pass underneath obstacles that do not intersect the ground. For more realism raycasts should be drawn from the head which would enable the height of the pedestrian and the direction they are facing to play a role in field of view.
1. Pedestrians do not detect vehicles and so do not use them for decision making.

## Debugging

The field of view can be seen visually around all pedestrians at all times when active and is seen as a white circle that surrounds a pedestrian.

Further debugging lines can be seen in the [SceneView](https://docs.unity3d.com/Manual/UsingTheSceneView.html) when a pedestrian is clicked on.
These lines are in red and display which other pedestrians are currently within the selected pedestrians `ViewAngle` and `ViewRadius`, meaning they are currently "seen" by the selected pedestrian.
