# Setup within Traffic3D

To use the Evacu-agent system follow the instructions below:

1. First, check out the `Evacu-agent_Test_Scene` scene for an example of the Evacu-agent pedestrians.
1. For Evacu-agent pedestrians to work on Traffic3D, the basic Traffic3D Pedestrian system must be implemented on your chosen scene, [click here](../traffic3d_pedestrians.md#setup) to find out how to setup Traffic3D Pedestrians.
1. Check that the scene has already been added to `File` > `Build Settings` > `Scenes in Build`. Click `Add Open Scenes` to add the active scene to the list if it is not present. Find the index number of the scene on the same page.
1. Find the script `PedestrianFactory` and edit the `evacuAgentBuildIndexes` list (which is done in the script itself) to include your build scene index number found in the step above.
1. Add an Evacu-agent factory type script to the `PedestrianFactory` game object such as `FriendGroupLeaderFollowerPedestrianFactory`, `WorkerLeaderFollowerPedestrianFactory`, etc.
1. In each factory script, place the relevant prefabs and behaviour collections which can be found in the `Assets/Resources/EvacuAgent/Prefabs` folder.
1. Through each `PedestrianPoint` game object, add a pedestrian point type script such as `ShoppingPedestrianPoint`, `WorkPedestrianPoint`, etc. Make sure you create at least one pedestrian point type for each of the pedestrian types e.g. Workers need at least one `WorkPedestrianPoint`.
1. Run the simulation and Evacu-agent Pedestrians should spawn on the relevant `PedestrianPoint`s.