# Working with random numbers

## Random number generator

The `RandomNumberGenerator` class is a singleton with a global seed for generating random numbers and a `System.Random` object.
This class ensures repeatability throughout the simulation and should be used instead of the `UnityEngine.Random` class.

An example below shows how to get a random `float` from a range of 1 to 5.

```cs
float min = 1f;
float max = 5f;
float randomFloat = RandomNumberGenerator.GetInstance().Range(min, max);
```