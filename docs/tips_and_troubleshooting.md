# Tips and troubleshooting

## Tips

* **Open Street Map imports can be manually edited** - It is best to edit roads, junctions, and furniture to suit your needs. Some junctions also need to be manually added or edited to mimic the junction in the real world.
* **Click on roads in Open Street Maps to find the road nodes that need moving** - To easily edit roads in OSM, click on the roadway. In the roadway script, double-click on the node that you need to move, and you will instantly select that node. This node can now be easily moved using Unity’s tools. Make sure to [re-render the road mesh](import_open_street_map.md#road-mesh-updater) once you have moved the nodes.
* **Prefabs help speed up scene builds** - Prefabs can be found in `Assets/Resources/Models` which are pre-made objects such as [traffic lights](traffic3d_assets.md#traffic-lights) and streetlights. If you need to make your own custom objects, these could also be used as a reference.
* **Display the network** - By clicking on each road, a line is painted along the road to show the shape of the current road selected. If that road is under a parent (in OSM all roads are created under one parent object), by clicking the parent object all roads will be highlighted to show the different components of the network. Clicking on a vehicle will also display its main path throughout the network and find its origin and destination. The direction of travel is determined by checking for a sphere wireframe on each node. The first node (the starting node) does not have a sphere wireframe attached. The rest of the nodes will have a sphere wireframe including the end node. You can see this in the following example: ![img.png](assets/images/DeterminePathDirection.png)

## Troubleshooting

### Vehicles are going the wrong way on a junction in Open Street Maps

**Solution**: Find which section of road the vehicles are going the wrong way on. All possible roadways are created within junctions and rules such as "no turning left" are not imported, so this might be why. Once you find the extra roadway simply delete it and make sure there are no more connections. Do not do this in play mode as Unity undoes all edits in that mode.

### Pedestrians are not spawning

**Solution**: Check that the path where the pedestrians should spawn is tagged with `pathway`. Without this tag, pedestrians will not be able to travel on that path and therefore be unable to spawn. Also check that your pedestrian point has the `PedestrianPoint` script and is in the correct location as it will find the nearest walkable area.

### Some vehicles do not always follow the roadway accurately

**Solution**: Vehicles normally look for road nodes rather than the line of the roadway (this will be improved in the future), so to make it more accurate, add more road nodes to a roadway and make corners and turnings smoother with multiple road nodes to make it easier for the vehicle to follow.

### The built software isn't spawning any vehicles into the scene and traffic lights are not working

**Solution**: Transfer all assets, except for scripts, from this project to a new project.
The scripts can be transferred only by creating a new script and then copying and pasting the old contents of that particular script.
References will also have to be amended as they will be broken during the transfer.
