# Getting started

## Clone the Traffic3D repository

Either download the code directly from the [project page](https://gitlab.com/traffic3d/traffic3d), or use [git](https://git-scm.com/) to clone the repository:

```sh
git@gitlab.com:traffic3d/traffic3d.git
cd traffic3d
```

## Installation and set-up

The project runs on `Unity 2018.3.11f1` and up.
Download the latest of Unity from the following link: [https://unity3d.com/get-unity/download/](https://unity3d.com/get-unity/download/)
Or download Unity 2018.3.11f1 directly from the following link: [https://unity3d.com/get-unity/download/archive](https://unity3d.com/get-unity/download/archive)

Use a preferred C# IDE or download [Visual Studio](https://visualstudio.microsoft.com/vs/) using the following link.
