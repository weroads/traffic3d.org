# Working with Unity

## Open the project

Open Unity and press the open button as shown below.

![Open Project Image](./assets/images/OpenProject.png)

Navigate to the location of the project select the folder that is the parent folder of both the `Assets` and `ProjectSettings` folder.

## Quick Unity overview

### Project window

All `Assets` can be access in this area.
This includes scripts and models for the simulation.

![Project Window Image](./assets/images/ProjectWindow.png)

### Hierarchy window

The scenes `assets` are listed here, each object may have a child object depending on the `Asset`.

![Hierarchy Window Image](./assets/images/HierarchyWindow.png)

### Scene window

Displays the scene and objects within that scene.
Moving around the scene can be done by holding right-click, turning the mouse and using `W` (forward) `S` (backward) `A` (left) and `D` (right).

![Scene Window Image](./assets/images/SceneWindow.png)

### Inspector window

Once an object has been clicked from either the scene or the hierarchy window, the **Inspector** window will show all the properties of that object.

![Inspector Window Image](./assets/images/InspectorWindow.png)

## Open a scene

In the navigation bar click on **File > Open Scene** and then click on the scene to open.
Scene files have the extension `.unity`.
The scene should then open into the **Scene** window.

![Open Scene Image](./assets/images/OpenScene.png)

## Edit properties

Navigate to the **Hierarchy** window and click on the object that needs to have its values changed.
As an example, car has been selected below.

![Select Car Image](./assets/images/SelectCar.png)

The **inspector** window then has the object's properties which can be changed by clicking on the field and typing in the new value.
For fields with an object as a value, click on the circle icon to the right of the field and select the new object for the field.

![Edit Car Image](./assets/images/EditCar.png)
