## New page

[[_TOC_]]

---

Briefly describe the new page you wish to add to the documentation.
Please keep in mind, that while we want Beautiful Canoe documentation to be comprehensive, we also want it to be readable and approachable.
Wherever possible, we prefer to add to existing documentation rather than create new pages here.

### Audience

* **Who is the intended audience for the new page?** e.g. Summer Fellows, maintenance contract developers, ...
* **What value do you expect them to find in the new information?**

### Content

What content should the new page have?

### Navigation

Where in the navigation structure of the website should your page appear?

* [ ] Root
* [ ] Start here
* [ ] Using Git
* [ ] Web security
* [ ] PHP projects
* [ ] Unity Projects
* [ ] Phone apps
* [ ] Static websites
* [ ] Deployment
* [ ] Starting a new project
* [ ] Server management (full time staff only)

If your proposal requires a new section in the navigation structure, please justify why that is necessary.

/label ~"Feature Request"
/label ~"Priority:Medium"
